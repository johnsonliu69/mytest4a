//
//  StepsViewController.swift
//  MyTest2
//
//  Created by Johnson Liu on 6/16/22.
//

import UIKit

class StepsViewController: UIViewController {
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var displayLabel: UILabel!
    
    var indexNumber: Int = 0
    var header: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = header
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        innerView.layer.borderColor = UIColor.black.cgColor
        innerView.layer.borderWidth = 0.5
        
        displayLabel.alpha = 0.5
        
        switch indexNumber {
        case 0:
            changeBgColor(color: UIColor.red)
            break
        case 1:
            changeBgColor(color: UIColor.green)
            break
        case 2:
            changeBgColor(color: UIColor.blue)
            break
        case 3:
            changeBgColor(color: UIColor.orange)
            break
        default:
            changeBgColor(color: UIColor.clear)
            break
        }
    }
    
    private func changeBgColor(color: UIColor) {
        displayLabel.backgroundColor = color
    }
}
